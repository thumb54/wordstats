﻿using NUnit.Framework;

namespace WordStats.WordStatsTest
{
    [TestFixture]
    class WordStatsReaderTests
    {
        [Test]
        public void ReadFromFile_BasicResultsSet()
        {
            var results = WordStatsReader.ReadFromFile("TheRailwayChildren.txt");

            //Test
            Assert.NotNull(results);
            Assert.NotNull(results.Results);
        }

        [Test]
        public void ReadFromFile_ContainsCommonWords()
        {
            var results = WordStatsReader.ReadFromFile("TheRailwayChildren.txt");

            //Test
            Assert.Greater(results.Results["the"].Occurences,100);
            Assert.Greater(results.Results["and"].Occurences,100);
        }

        [Test]
        public void ReadFromFile_DoesNotContainsInvalidKeys()
        {
            var results = WordStatsReader.ReadFromFile("TheRailwayChildren.txt");

            //Test
            Assert.IsFalse(results.Results.ContainsKey(""));
            Assert.IsFalse(results.Results.ContainsKey(" "));
            Assert.IsFalse(results.Results.ContainsKey("-"));
        }

        [Test]
        public void ReadFromFile_DoesNotContainsCommonWordsCapitals()
        {

            var results = WordStatsReader.ReadFromFile("TheRailwayChildren.txt");

            //Test
            Assert.IsFalse(results.Results.ContainsKey("The"));
            Assert.IsFalse(results.Results.ContainsKey("THE"));
            Assert.IsFalse(results.Results.ContainsKey("AND"));
            Assert.IsFalse(results.Results.ContainsKey("And"));
        }

        [Test]
        public void ReadFromFile_ProcessStatsSet()
        {
            var results = WordStatsReader.ReadFromFile("TheRailwayChildren.txt");

            //Test
            Assert.NotNull(results.ProcessMemoryUse);
            //Cannot test memory as garbage collection may run and make this number negative
            //Assert.Greater(results.ProcessMemoryUse,0);
            Assert.NotNull(results.ProcessTime);
            Assert.Greater(results.ProcessTime,0);
        }
    }
}
