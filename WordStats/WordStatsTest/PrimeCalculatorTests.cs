﻿using System;
using NUnit.Framework;
namespace WordStats.WordStatsTest
{
    [TestFixture]
    public class PrimeCalculatorTests
    {
        [Test]
        public void PrimeCalculator_TestFirst10Primes()
        {
            //Setup
            var primeCalculator = new PrimeCalculator();

            //Test first 10 known primes
            Assert.IsTrue(primeCalculator.IsPrime(2));
            Assert.IsTrue(primeCalculator.IsPrime(3));
            Assert.IsTrue(primeCalculator.IsPrime(5));
            Assert.IsTrue(primeCalculator.IsPrime(7));
            Assert.IsTrue(primeCalculator.IsPrime(11));
            Assert.IsTrue(primeCalculator.IsPrime(13));
            Assert.IsTrue(primeCalculator.IsPrime(17));
            Assert.IsTrue(primeCalculator.IsPrime(19));
            Assert.IsTrue(primeCalculator.IsPrime(23));
            Assert.IsTrue(primeCalculator.IsPrime(29));
        }
        [Test]
        public void PrimeCalculator_TestFirst10NonPrimes()
        {
            //Setup
            var primeCalculator = new PrimeCalculator();

            //Test first 10 known non-primes
            Assert.IsFalse(primeCalculator.IsPrime(1));
            Assert.IsFalse(primeCalculator.IsPrime(4));
            Assert.IsFalse(primeCalculator.IsPrime(6));
            Assert.IsFalse(primeCalculator.IsPrime(8));
            Assert.IsFalse(primeCalculator.IsPrime(9));
            Assert.IsFalse(primeCalculator.IsPrime(12));
            Assert.IsFalse(primeCalculator.IsPrime(14));
            Assert.IsFalse(primeCalculator.IsPrime(15));
            Assert.IsFalse(primeCalculator.IsPrime(16));
            Assert.IsFalse(primeCalculator.IsPrime(18));
        }

        [Test]
        public void PrimeCalculator_TestHigherPrimes()
        {
            //Setup
            var primeCalculator = new PrimeCalculator();

            //Test known higher primes
            Assert.IsTrue(primeCalculator.IsPrime(5479));
            Assert.IsTrue(primeCalculator.IsPrime(6203));
            Assert.IsTrue(primeCalculator.IsPrime(6311));
            Assert.IsTrue(primeCalculator.IsPrime(6451));
            Assert.IsTrue(primeCalculator.IsPrime(7867));
            Assert.IsTrue(primeCalculator.IsPrime(7853));
            Assert.IsTrue(primeCalculator.IsPrime(7919));
        }

        [Test]
        public void PrimeCalculator_TestHigherNonPrimes()
        {
            //Setup
            var primeCalculator = new PrimeCalculator();

            //Test known higher primes
            Assert.IsFalse(primeCalculator.IsPrime(5228));
            Assert.IsFalse(primeCalculator.IsPrime(5502));
            Assert.IsFalse(primeCalculator.IsPrime(6354));
            Assert.IsFalse(primeCalculator.IsPrime(6452));
            Assert.IsFalse(primeCalculator.IsPrime(6570));
            Assert.IsFalse(primeCalculator.IsPrime(7880));
            Assert.IsFalse(primeCalculator.IsPrime(7918));
        }


        [Test]
        public void PrimeCalculator_NegativeThrowsArgumentException()
        {

            //Setup
            var primeCalculator = new PrimeCalculator();

            //Test negatives (not generally thought of as being prime)
            Assert.Throws(typeof(ArgumentException), () => primeCalculator.IsPrime(-1));
            Assert.Throws(typeof(ArgumentException), () => primeCalculator.IsPrime(-7918));
        }

        [Test]
        public void PrimeCalculator_ZeroIsPrime()
        {

            //Setup
            var primeCalculator = new PrimeCalculator();

            //Test Zero is prime
            Assert.IsTrue(primeCalculator.IsPrime(0));
        }

        [Test]
        public void PrimeCalculator_SaveCalculatedPrimesGivesSameResults()
        {

            //Setup
            var primeCalculator = new PrimeCalculator();
            var primeCalculatorWithSaveOption = new PrimeCalculator(new PrimeCalculatorOptions()
            {
                SaveCalculatedPrimes = true
            });

            //Test first 10 known primes
            Assert.IsTrue(primeCalculator.IsPrime(2));
            Assert.IsTrue(primeCalculator.IsPrime(3));
            Assert.IsTrue(primeCalculator.IsPrime(5));
            Assert.IsTrue(primeCalculator.IsPrime(7));
            Assert.IsTrue(primeCalculator.IsPrime(11));
            Assert.IsTrue(primeCalculator.IsPrime(13));
            Assert.IsTrue(primeCalculator.IsPrime(17));
            Assert.IsTrue(primeCalculator.IsPrime(19));
            Assert.IsTrue(primeCalculator.IsPrime(23));
            Assert.IsTrue(primeCalculator.IsPrime(29));

            //Test first 10 known primes with save option
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(2));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(3));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(5));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(7));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(11));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(13));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(17));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(19));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(23));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(29));


            //Test known higher primes
            Assert.IsTrue(primeCalculator.IsPrime(5479));
            Assert.IsTrue(primeCalculator.IsPrime(6203));
            Assert.IsTrue(primeCalculator.IsPrime(6311));
            Assert.IsTrue(primeCalculator.IsPrime(6451));
            Assert.IsTrue(primeCalculator.IsPrime(7867));
            Assert.IsTrue(primeCalculator.IsPrime(7853));
            Assert.IsTrue(primeCalculator.IsPrime(7919));

            //Test known higher primes with save option
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(5479));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(6203));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(6311));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(6451));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(7867));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(7853));
            Assert.IsTrue(primeCalculatorWithSaveOption.IsPrime(7919));
        }


        [Test]
        public void PrimeCalculator_EratosthenesSieveInitialPrimeCalculationGivesSameResults()
        {

            //Setup
            var primeCalculator = new PrimeCalculator();
            var primeCalculatorEratosthenes = new PrimeCalculator(new PrimeCalculatorOptions()
            {
                InitialPrimeCalculation = PrimeCalculationMethod.EratosthenesSieve
            });



            //Test first 10 known primes
            Assert.IsTrue(primeCalculator.IsPrime(2));
            Assert.IsTrue(primeCalculator.IsPrime(3));
            Assert.IsTrue(primeCalculator.IsPrime(5));
            Assert.IsTrue(primeCalculator.IsPrime(7));
            Assert.IsTrue(primeCalculator.IsPrime(11));
            Assert.IsTrue(primeCalculator.IsPrime(13));
            Assert.IsTrue(primeCalculator.IsPrime(17));
            Assert.IsTrue(primeCalculator.IsPrime(19));
            Assert.IsTrue(primeCalculator.IsPrime(23));
            Assert.IsTrue(primeCalculator.IsPrime(29));

            //Test first 10 known primes with Eratosthenes precalculation
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(2));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(3));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(5));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(7));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(11));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(13));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(17));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(19));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(23));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(29));


            //Test known higher primes
            Assert.IsTrue(primeCalculator.IsPrime(5479));
            Assert.IsTrue(primeCalculator.IsPrime(6203));
            Assert.IsTrue(primeCalculator.IsPrime(6311));
            Assert.IsTrue(primeCalculator.IsPrime(6451));
            Assert.IsTrue(primeCalculator.IsPrime(7867));
            Assert.IsTrue(primeCalculator.IsPrime(7853));
            Assert.IsTrue(primeCalculator.IsPrime(7919));

            //Test known higher primes with Eratosthenes precalculation
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(5479));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(6203));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(6311));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(6451));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(7867));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(7853));
            Assert.IsTrue(primeCalculatorEratosthenes.IsPrime(7919));

        }


        [Test]
        public void PrimeCalculator_KnownInitialPrimeCalculationGivesSameResults()
        {

            //Setup
            var primeCalculator = new PrimeCalculator();
            var primeCalculatorKnownPrimes = new PrimeCalculator(new PrimeCalculatorOptions()
            {
                InitialPrimeCalculation = PrimeCalculationMethod.KnownPrimes
            });



            //Test first 10 known primes
            Assert.IsTrue(primeCalculator.IsPrime(2));
            Assert.IsTrue(primeCalculator.IsPrime(3));
            Assert.IsTrue(primeCalculator.IsPrime(5));
            Assert.IsTrue(primeCalculator.IsPrime(7));
            Assert.IsTrue(primeCalculator.IsPrime(11));
            Assert.IsTrue(primeCalculator.IsPrime(13));
            Assert.IsTrue(primeCalculator.IsPrime(17));
            Assert.IsTrue(primeCalculator.IsPrime(19));
            Assert.IsTrue(primeCalculator.IsPrime(23));
            Assert.IsTrue(primeCalculator.IsPrime(29));

            //Test first 10 known primes with Known Prime precalculation
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(2));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(3));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(5));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(7));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(11));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(13));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(17));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(19));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(23));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(29));


            //Test known higher primes
            Assert.IsTrue(primeCalculator.IsPrime(5479));
            Assert.IsTrue(primeCalculator.IsPrime(6203));
            Assert.IsTrue(primeCalculator.IsPrime(6311));
            Assert.IsTrue(primeCalculator.IsPrime(6451));
            Assert.IsTrue(primeCalculator.IsPrime(7867));
            Assert.IsTrue(primeCalculator.IsPrime(7853));
            Assert.IsTrue(primeCalculator.IsPrime(7919));

            //Test known higher primes with Known Prime precalculation
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(5479));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(6203));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(6311));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(6451));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(7867));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(7853));
            Assert.IsTrue(primeCalculatorKnownPrimes.IsPrime(7919));
        }
    }
}
