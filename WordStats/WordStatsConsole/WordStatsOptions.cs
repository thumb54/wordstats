﻿using CommandLine;
using CommandLine.Text;

namespace WordStats.WordStatsConsole
{
    class WordStatsOptions
    {
        [Option('i', Required = true, HelpText = "Input file to be processed.")]
        public string InputFile { get; set; }

        [Option('a', DefaultValue = false, MutuallyExclusiveSet = "show", HelpText = "Prints all word statistics to standard output.")]
        public bool ShowAll { get; set; }

        [Option('t', DefaultValue = 20, MutuallyExclusiveSet = "show", HelpText = "Prints only the top X word statistics to standard output.")]
        public int ShowTopX { get; set; }

        [Option('d', DefaultValue = false, MutuallyExclusiveSet = "calcmethod", HelpText = "Whether to disable saving primes found previously by the calculator.")]
        public bool DisableSavePrimesFound { get; set; }

        [Option('e', DefaultValue = false, MutuallyExclusiveSet = "calcmethod", HelpText = "Use Eratosthenes prime calculation for low values.")]
        public bool UseEratosthenesPrimePreCalculation { get; set; }

        [Option('k', DefaultValue = false, MutuallyExclusiveSet = "calcmethod", HelpText = "Use known prime calculation for low values.")]
        public bool UseKnownPrimePrimePreCalculation { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
                (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}