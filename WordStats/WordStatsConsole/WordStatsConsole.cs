﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using CommandLine;

namespace WordStats.WordStatsConsole
{
    public class WordStatsConsole
    {
        public static void Main(string[] args)
        {
            var options = new WordStatsOptions();


            var commandLineParser = new  Parser(new ParserSettings
            {
                MutuallyExclusive = true,
                CaseSensitive = false,
                HelpWriter = Console.Error
            });

            if (commandLineParser.ParseArguments(args, options))
            {
                //Configure primeCalculatorOptions
                var primeCalculationMethod = PrimeCalculationMethod.None;
                if (options.UseEratosthenesPrimePreCalculation == true)
                {
                    primeCalculationMethod = PrimeCalculationMethod.EratosthenesSieve;
                }
                else
                {
                    if (options.UseKnownPrimePrimePreCalculation==true)
                    primeCalculationMethod = PrimeCalculationMethod.KnownPrimes;
                }

                var primeCalculatorOptions = new PrimeCalculatorOptions
                {
                    SaveCalculatedPrimes = !options.DisableSavePrimesFound,
                    InitialPrimeCalculation = primeCalculationMethod
                };

                Console.WriteLine("Options:\n");
                Console.WriteLine("Input File : {0}", options.InputFile);
                Console.WriteLine("DisableSavePrimesFound : {0}", options.DisableSavePrimesFound);
                Console.WriteLine("Prime Calculation Method : {0}", primeCalculatorOptions.InitialPrimeCalculation);
                Console.WriteLine("");

                //Analyse
                Console.WriteLine("Analysing file {0}...", options.InputFile);
                var results = WordStatsReader.ReadFromFile(options.InputFile, primeCalculatorOptions);
                
                //Order the results
                var resultsOrdered = from r in results.Results orderby r.Value.Occurences descending select r;

                // Output the results
                if (options.ShowAll)
                {
                    Console.WriteLine("All words:\n");
                    PrintResults(resultsOrdered);
                }
                else
                {
                    Console.WriteLine("Most common {0} words:\n", options.ShowTopX);
                    var top100Words = resultsOrdered.Take(options.ShowTopX);
                    PrintResults(top100Words);
                }

                //Completion stats
                Console.WriteLine();
                Console.WriteLine("Completed in {0} Ticks", results.ProcessTime);
                Console.WriteLine("{0} Bytes used (estimated)", results.ProcessMemoryUse);
                Console.WriteLine();
            }
        }

        private static void PrintResults(IEnumerable<KeyValuePair<string, WordUsageResult>> resultsOrdered)
        {
            foreach (var wordStat in resultsOrdered)
            {
                var primeText = wordStat.Value.OccurencesCountIsPrimeNumber ? "- PRIME" : string.Empty;

                Console.WriteLine("{0} - {1}{2} ", wordStat.Value.Occurences, wordStat.Key, primeText);
            }
        }
    }
}
