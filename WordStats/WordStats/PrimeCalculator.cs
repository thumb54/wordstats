﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WordStats
{
    public class PrimeCalculator
    {
        private readonly PrimeCalculatorOptions _options;
        private readonly IDictionary<int,bool?> _knownPrimes = new Dictionary<int, bool?>();
        
        public PrimeCalculator(PrimeCalculatorOptions options = null)
        {
            _options = options ?? new PrimeCalculatorOptions() { InitialPrimeCalculation = PrimeCalculationMethod.None, SaveCalculatedPrimes = true };

            switch (_options.InitialPrimeCalculation)
            {
                case PrimeCalculationMethod.EratosthenesSieve:
                    PopulatePrimeUsingEratosthenesSieve(_knownPrimes);
                    break;
                case PrimeCalculationMethod.KnownPrimes:
                    PopulatePrimeUsingKnownPrimes(ref _knownPrimes);
                    break;
                case PrimeCalculationMethod.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void PopulatePrimeUsingKnownPrimes(ref IDictionary<int, bool?> knownPrimes)
        {
            //Why compute primes at run-time when they could be loaded into memory from a source?
            var jsonString = File.ReadAllText("InitialPrimes.json");
            knownPrimes = JsonConvert.DeserializeObject<IDictionary<int, bool?>>(jsonString);
            
        }

        private void PopulatePrimeUsingEratosthenesSieve(IDictionary<int, bool?> knownPrimes)
        {
            //A basic yet fairly efficient method of prime calculation for low primes
            //Iiteratively marks as composite (i.e., not prime) the multiples of each prime, starting with the multiples of 2

            // Eratosthenes' method:

            //Create a list of consecutive integers from 2 through n: (2, 3, 4, ..., n).
            var n = 10000;
            for (var i = 2; i <= n; i++)
                knownPrimes[i] = null;
            
            //Initially, let p equal 2, the first prime number.
            var p = 2;
            EratosthenesSieveRecursive(p, n, knownPrimes);

            //Then set all the unmarked as prime
            var nullPrimes = knownPrimes.Where(k => k.Value == null).ToArray();
            foreach (var kp in nullPrimes)
            {
                knownPrimes[kp.Key] = true;
            }
        }

        private void EratosthenesSieveRecursive(int p, int n, IDictionary<int, bool?> eratosthenesPrimes)
        {
            //Starting from p, enumerate its multiples by counting to n in increments of p, and mark them in the list (these will be 2p, 3p, 4p, ... ; the p itself should not be marked).
            for (var i = p; i <= n; i += p)
            {
                if (i != p)
                {
                    eratosthenesPrimes[i] = false;
                }
            }

            //Find the first number greater than p in the list that is not marked. If there was no such number, stop. Otherwise, let p now equal this new number (which is the next prime), and repeat from step 3.
            for (var i = p + 1; i <= n; i++)
            {
                if (eratosthenesPrimes[i] == null)
                {
                    //Unmarked number found
                    EratosthenesSieveRecursive(i, n, eratosthenesPrimes);
                    break;
                }
            }
        }

        public bool IsPrime(int input)
        {
            if (input<0)
                throw new ArgumentException("Prime numbers must be positive, greater than zero");

            if (_knownPrimes.Any())
            {
                if (_knownPrimes.ContainsKey(input) )
                    return _knownPrimes[input].Value;
            }

            for (var i = 2; i < input+1/2; i++)
            {
                if (input%i == 0)
                {
                    if (_options.SaveCalculatedPrimes)
                    {

                        //Divisor found
                        _knownPrimes[input] = false;
                        return false;
                    }
                }
            }
            //Divisor not found
            var result = input>1 || input ==0;
            if (_options.SaveCalculatedPrimes)
            {
                _knownPrimes[input] = result;
            }
            return result;

        }
    }

    public class PrimeCalculatorOptions
    {
        public bool SaveCalculatedPrimes { get; set; }
        public PrimeCalculationMethod InitialPrimeCalculation { get; set; }

    }

    public enum PrimeCalculationMethod
    {
        None,
        EratosthenesSieve,
        KnownPrimes //Is this cheating?
        
    }
}
