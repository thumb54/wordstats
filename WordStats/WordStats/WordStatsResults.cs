﻿using System.Collections.Generic;

namespace WordStats
{
    public class WordStatsResults
    {
        /// <summary>
        /// List of words and number of occurrences
        /// </summary>
        public IDictionary<string, WordUsageResult> Results { get; set; }
        
        /// <summary>
        /// Time in ns for the operation
        /// </summary>
        public long ProcessTime { get; set; }

        /// <summary>
        /// Memory usage (bytes)
        /// </summary>
        public long ProcessMemoryUse { get; set; }

    }
}
