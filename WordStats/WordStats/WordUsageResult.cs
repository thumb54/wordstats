namespace WordStats
{
    public class WordUsageResult
    {
        public int Occurences { get; set; }
        public bool OccurencesCountIsPrimeNumber { get; set; }
    }
}