﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WordStats
{
    public static class WordStatsReader
    {

        public static WordStatsResults ReadFromFile(string filePath, PrimeCalculatorOptions primeCalculatorOptions=null)
        {
            var wordCounts = new Dictionary<string,WordUsageResult>();
            var startTime = DateTime.Now;
            var startMemory = GC.GetTotalMemory(false);
            var primeCalculator = new PrimeCalculator();

            //Process the file and populate the stats
            using (var fileStream = new FileStream(filePath,
                FileMode.Open,
                FileAccess.Read,
                FileShare.None))
            {
                 using (var streamReader = new StreamReader(fileStream))
                 {
                     string lineOfText;
                     //Read line by line
                     while ((lineOfText = streamReader.ReadLine()) != null)
                     {
                         //Get each word on the line
                         var wordsOnLine = lineOfText.Split(new[] {" ", "--"},StringSplitOptions.RemoveEmptyEntries);
                         
                         foreach (var word in wordsOnLine)
                         {
                             //Remove puctuation, special characters and make lower case
                             var wordCleaned = CleanWord(word);
                             if (string.IsNullOrEmpty(wordCleaned)) continue;

                             if (wordCounts.ContainsKey(wordCleaned))
                             {
                                 //Existing word -- update count
                                 wordCounts[wordCleaned].Occurences++;
                             }
                             else
                             {
                                 //New word -- Add to list
                                 wordCounts[wordCleaned] = new WordUsageResult() {Occurences = 1};
                             }
                         }
                     }
                 }
            }
            //Populate whether occurences counts are prime
            foreach (var wordCount in wordCounts)
            {
                wordCount.Value.OccurencesCountIsPrimeNumber = primeCalculator.IsPrime(wordCount.Value.Occurences);
            }

            //Return results
            var results = new WordStatsResults {Results = wordCounts};

            //End Memory
            var endTime = DateTime.Now;
            var endMemory = GC.GetTotalMemory(false);

            results.ProcessMemoryUse = endMemory - startMemory;
            results.ProcessTime = (endTime - startTime).Ticks;

            return results;
        }

        private static string CleanWord(string input)
        {
            return new string(input.Where(c => !char.IsPunctuation(c)).ToArray()).ToLower();
        }
    }
}
