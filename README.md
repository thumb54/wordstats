# README #

This application counts the number of unique words occurring in a text document and whether the number of occurrences is prime.


### How do I get set up? ###

Please clone the repository, build and use the console application as below:

"WordStatsConsole - i [FileName]"

For full report of all words use
"WordStatsConsole - i [FileName] -a"

Or for top X words use
"WordStatsConsole - i [FileName] -t X"

To disable saving known primes (slower) use
"WordStatsConsole - i [FileName] - d"

To use Eratosthenes prime pre-calculation use
"WordStatsConsole - i [FileName] - e"

To use known prime pre-calculation use
"WordStatsConsole - i [FileName] - k